<div class="row">
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Block Height
        </p>
        <h4 class="card-number">
          <?php
            curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchBlock&blockID=" . urlencode($tx['blockhash']));
            $res = curl_exec($ch);
            $blockHeight = json_decode($res,1)['result']['height'];
            if(empty($blockHeight)){
              ?>
                ???
              <?php
            }else{
              ?>
                <?= htmlentities($blockHeight); ?>
              <?php
            }
          ?>
        </h4>
      </div>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Blockhash
        </p>
        <h4 class="card-number">
          <?php if(empty($tx['blockhash'])){ ?>
            ???
          <?php }else{ ?>
            <?= htmlentities(substr($tx['blockhash'],0,8)); ?>
          <?php } ?>
        </h4>
      </div>
    </div>
  </div>
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Confirmations
        </p>
        <h4 class="card-number"><?= htmlentities($tx['confirmations']); ?></h4>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content indigo white-text">
        <p class="card-title v-center">
          Summary
        </p>
        <table class="white-text responsive-table">
          <tbody>
            <tr>
              <td>Total Output</td>
              <td>
                <?php
                  $totalOutput = 0;
                  foreach($tx['vout'] as $key => $vout){
                    $totalOutput += $vout['value'];
                  }
                  echo $totalOutput;
                ?>
              </td>
            </tr>
            <tr>
              <td>Fees</td>
              <td>
                <?php
                  $totalInput = 0;
                  foreach($tx['vin'] as $key => $vin){
                    curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchTx&txID=" . urlencode($vin['txid']));
                    $res = curl_exec($ch);
                    $inputTx = json_decode($res,1)['result'];
                    foreach($inputTx['vout'] as $ikey => $ivout){
                      if($vin['vout'] == $ivout['n']){
                        $totalInput += $ivout['value'];
                        break;
                      }
                    }
                    //$totalOutput += $vout['value'];
                  }
                  echo round($totalInput - $totalOutput,11);
                ?>
              </td>
            </tr>
            <tr>
              <td>Timestamp</td>
              <td><?= date("Y-m-d h:i:s",$tx['time']); ?></td>
            </tr>
          </tbody>
        </table>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content indigo white-text">
        <?php
          foreach($tx['vin'] as $key => $vin){
            curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchTx&txID=" . urlencode($vin['txid']));
            $res = curl_exec($ch);
            $inputTx = json_decode($res,1);
            foreach($inputTx['vout'] as $ikey => $ivout){
              if($vin['vout'] == $ivout['n']){
                $totalInput += $ivout['value'];
                break;
              }
            }
            //$totalOutput += $vout['value'];
          }
        ?>
        <p class="card-title v-center">
          Inputs
        </p>
        <div class="s12 m6 l6">
          <table class="white-text responsive-table">
            <thead>
              <tr>
                <th>Index</th>
                <th>Previous Output</th>
                <th>Address</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($tx['vin'] as $key => $vin){
                  ?>
                    <tr>
                      <td><?= htmlentities($vin['vout']); ?></td>
                      <td><a class="white-text" href="<?= htmlentities($FinlayDaG33k->EzServer->getHome());?>/search/<?= htmlentities($vin['txid']); ?>"><?= htmlentities(substr($vin['txid'],0,16) . "..."); ?></a></td>
                      <td><?= htmlentities($ivout['scriptPubKey']['addresses'][0]); ?></td>
                      <td><?= htmlentities($ivout['value']); ?></td>
                    </tr>
                  <?php
                }
                ?>
            </tbody>
          </table>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col s12 m12 l12">
    <div class="card">
      <div class="card-content indigo white-text">
        <?php
          foreach($tx['vin'] as $key => $vin){
            curl_setopt($ch, CURLOPT_URL, $FinlayDaG33k->EzServer->getHome() . "/ajax/index.php?action=searchTx&txID=" . urlencode($vin['txid']));
            $res = curl_exec($ch);
            $inputTx = json_decode($res,1);
            foreach($inputTx['vout'] as $ikey => $ivout){
              if($vin['vout'] == $ivout['n']){
                $totalInput += $ivout['value'];
                break;
              }
            }
            //$totalOutput += $vout['value'];
          }
        ?>
        <p class="card-title v-center">
          Outputs
        </p>
        <div class="s12 m6 l6">
          <table class="white-text responsive-table">
            <thead>
              <tr>
                <th>Index</th>
                <th>Address</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach($tx['vout'] as $key => $vout){
                  ?>
                    <tr>
                      <td><?= htmlentities($vout['n']); ?></td>
                      <td><?= htmlentities($vout['scriptPubKey']['addresses'][0]); ?></td>
                      <td><?= htmlentities($vout['value']); ?></td>
                    </tr>
                  <?php
                }
                ?>
            </tbody>
          </table>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>
