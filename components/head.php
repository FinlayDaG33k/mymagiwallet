<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<link rel="stylesheet" href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/lib/css/style.css?nocache=<?= htmlentities($FinlayDaG33k->EzServer->randomStr()); ?>">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/lib/js/moment.min.js"></script>
<script src="https://use.fontawesome.com/250278b505.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
<script src="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/lib/js/qrcode.min.js"></script>
<script>var apiURL = "<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/ajax/index.php";</script>
<script>
var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}
</script>

<script>
  var UpdateUIItems = {
    getConnections:true,
    getMiningInfo:true,
    getTicker:false,
    getBalance:false,
    getTransactions:false,
    getWalletAddress:false
  };
</script>
