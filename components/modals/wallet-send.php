<div id="wallet-send" class="modal">
  <div class="modal-content">
    <h4>Send Magicoin</h4>
      <p>
        This function is still highly experimental and under development, use at own risk!<br />
      </p>
      <p>
        <form id="sendCoinForm" class="col s12 m12 l12">
          <div class="row">
            <div class="input-field col s12">
              <input id="recipient-address" type="text" name="recipient-address">
              <label for="recipient-address">Recipient Address</label>
              <small id="recipient-validity"></small>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="amount" type="text" class="validate" name="amount">
              <label for="amount">amount</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <textarea id="comment" name="comment" class="materialize-textarea"></textarea>
              <label for="amount">Optional Message</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="password" type="password" class="validate" name="password">
              <label for="password">Password</label>
              <small>For security reasons, enter your password</small>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>
              <button type="reset" class="btn waves-effect waves-light"><i class="fa fa-times" aria-hidden="true"></i> reset</button>
            </div>
          </div>
        </form>
      </p>
      <script>
        $(document).ready(function() {
          //setup before functions
          var typingTimer;                //timer identifier
          var doneTypingInterval = 1000;  //time in ms, 5 second for example
          var $input = $('#recipient-address');

          //on keyup, start the countdown
          $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
          });

          //on keydown, clear the countdown
          $input.on('keydown', function () {
            clearTimeout(typingTimer);
          });

          //user is "finished typing," do something
          function doneTyping () {
            //do something
            $.ajax({
              url: apiURL,
              method: "GET",
              data: "action=validateAddress&address=" + $('#recipient-address').val(),
              success: function (res) {
                console.log(res);
                data = JSON.parse(res);
                if(data.result.isvalid){
                  $('#recipient-address').addClass("valid");
                  $('#recipient-address').removeClass("invalid");
                  $("#recipient-validity").html("");
                }else{
                  $('#recipient-address').removeClass("valid");
                  $('#recipient-address').addClass("invalid");
                  $("#recipient-validity").html("Recipient address is invalid!");
                }
              }
            });
          }

          $("#sendCoinForm").submit(function(event) {
            event.preventDefault();
            $.ajax({
              url: apiURL,
              method: "POST",
              data: "action=sendCoins&" + $(this).serialize(),
              success: function (res) {
                console.log(res);
                data = JSON.parse(res);
                switch(data.status){
                  case 200:
                    Materialize.toast("Sucessfully send Σ " + escapeHtml($("#amount").val()) + " to " + escapeHtml($("#recipient-address").val()),4000);
                    $("sendCoinForm").reset();
                    break;
                  case 403:
                    switch(data.message){
                      case "Wallet Identifier can't be blank!":
                        break;
                      case "send amount to low!":
                        Materialize.toast("Minimum send amount is 0.001",4000);
                        break;
                      case "Invalid recipient!":
                        Materialize.toast(escapeHtml($("#recipient-address").val()) + "Is not a valid recipient!",4000);
                        break;
                      case "Invalid Wallet Identifier/Authtoken!":
                        break;
                      case "Invalid password!":
                        Materialize.toast("The password you've entered is invalid!",4000);
                        break;
                      case "Insufficient balance":
                        Materialize.toast("Insufficient balance to place this transaction!",4000);
                        break
                    }
                    break;
                }
              }
            });
          });
        });
      </script>
  </div>
</div>
