<div id="import-key" class="modal">
  <div class="modal-content">
    <h4>Import Key</h4>
      <p>
        Already had a wallet using m-wallet?<br />
        You can now import your keys to use them in MyMagiWallet!<br />
        Keys can be retreived from m-wallet by running the following command (replace <code>&lt;address&gt;</code> with your Magi Address):<br />
        <span id="export-key-command">dumpprivkey &lt;address&gt;</span>
        <button class="btn waves-effect waves-light clipboard-btn" data-clipboard-target="#export-key-command">Copy</button>
        <br />
        This function is still highly experimental and under development, use at own risk!
      </p>
      <p>
        <form id="importKeyForm" class="col s12 m12 l12">
          <div class="row">
            <div class="input-field col s12">
              <input id="key" type="text" name="key">
              <label for="key">Enter Private Key</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>
              <button type="reset" class="btn waves-effect waves-light"><i class="fa fa-times" aria-hidden="true"></i> reset</button>
            </div>
          </div>
        </form>
      </p>
      <script>
        $(document).ready(function() {
          new Clipboard('.clipboard-btn');
          $("#importKeyForm").submit(function(event) {
            event.preventDefault();
            $.ajax({
              url: apiURL,
              method: "POST",
              data: "action=importKey&" + $(this).serialize(),
              success: function (res) {
                console.log(res);
                data = JSON.parse(res);
                if(data.status == 200){
                  Materialize.toast('Sucessfully imported the key!', 4000);
                }else{
                  Materialize.toast(data.message, 4000);
                }
              }
            });
          });
        });
      </script>
  </div>
</div>
