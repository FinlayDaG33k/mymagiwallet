<div id="change-password" class="modal">
  <div class="modal-content">
    <h4>Change Password</h4>
      <p>
        This function is still highly experimental and under development, use at own risk!
      </p>
      <p>
        <form id="changePasswordForm" class="col s12 m12 l12">
          <div class="row">
            <div class="input-field col s12">
              <input id="old-password" type="password" name="old-password">
              <label for="old-password">Current Password</label>
              <small id="old-password"></small>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="new-password" type="password" name="new-password">
              <label for="new-password">New Password</label>
              <small id="new-password"></small>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="new-password-conf" type="password" class="validate" name="new-password-conf">
              <label for="new-password-conf">Confirm New Password</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit</button>
              <button type="reset" class="btn waves-effect waves-light"><i class="fa fa-times" aria-hidden="true"></i> reset</button>
            </div>
          </div>
        </form>
      </p>
      <script>
        $(document).ready(function() {
          $("#changePasswordForm").submit(function(event) {
            event.preventDefault();
            if($("#new-password").val() == $("#new-password-conf").val()){
              $.ajax({
                url: apiURL,
                method: "POST",
                data: "action=changePassword&" + $(this).serialize(),
                success: function (res) {
                  data = JSON.parse(res);
                  if(data.status == 200){
                    Materialize.toast('Password changed successfully!', 4000);
                  }else{
                    Materialize.toast(data.message, 4000);
                  }
                }
              });
            }else{
              Materialize.toast('Password does not match the confirmation password', 4000);
            }
          });
        });
      </script>
  </div>
</div>
