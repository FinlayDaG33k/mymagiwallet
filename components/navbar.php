<nav>
  <div class="nav-wrapper indigo">
    <?php if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){ ?>
      <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/wallet" class="brand-logo">MyMagiWallet</a>
    <?php }else{ ?>
      <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>" class="brand-logo">MyMagiWallet</a>
    <?php } ?>
    <a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons">menu</i></a>
    <ul id="nav-mobile" class="right hide-on-med-and-down">
      <li>
        <a id="navbtn_refreshing" href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button tooltipped" data-position="bottom" data-delay="50" data-tooltip="Refreshing all the data, please hang on tight!">
          <i class="fa fa-cog fa-spin"></i>
        </a>
      </li>
      <li>
        <a id="navbtn_connectionCount" href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button tooltipped" data-position="bottom" data-delay="50">
          <i class="fa fa-connectdevelop left" aria-hidden="true"></i><span id="nav_connectionCount">???</span>
        </a>
      </li>
      <li>
        <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="network-info-dropdown">
          <i class="fa fa-cogs" aria-hidden="true"></i>
        </a>
      </li>

      <li>
        <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="user-dropdown">
          <i class="fa fa-power-off" aria-hidden="true"></i>
        </a>
      </li>
    </ul>
    <!-- Mobile navbar -->
    <ul class="side-nav" id="mobile-nav">
      <li>
        <a id="navbtn_refreshing-mobile" href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button tooltipped" data-position="bottom" data-delay="50" data-tooltip="Refreshing all the data, please hang on tight!">
          <i class="fa fa-cog fa-spin"></i>
        </a>
      </li>
      <li>
        <a id="navbtn_connectionCount-mobile" href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button tooltipped" data-position="bottom" data-delay="50">
          <i class="fa fa-connectdevelop left" aria-hidden="true"></i><span id="nav_connectionCount-mobile">???</span>
        </a>
      </li>
      <li>
        <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="network-info-dropdown-mobile">
          <i class="fa fa-cogs" aria-hidden="true"></i>  Network Info
        </a>
      </li>

      <li>
        <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="user-dropdown-mobile">
          <i class="fa fa-power-off" aria-hidden="true"></i>  My Wallet
        </a>
      </li>
      </ul>
  </div>
</nav>

<ul id="network-info-dropdown" class="dropdown-content">
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-code-fork" aria-hidden="true"></i>Magid Version: <span id="nav_WalletVersion">???</span>
    </a>
  </li>
  <li class="divider"></li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-link" aria-hidden="true"></i><span id="nav_blockCount">???</span> Blocks
    </a>
  </li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-cogs" aria-hidden="true"></i>diff POW: <span id="nav_diffPoW">???</span>
    </a>
  </li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-cogs" aria-hidden="true"></i>diff POS: <span id="nav_diffPoS">???</span>
    </a>
  </li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-usd" aria-hidden="true"></i>Block Value: Σ <span id="nav_blockValue">???</span>
    </a>
  </li>
</ul>
<?php if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){ ?>
  <ul id="user-dropdown" class="dropdown-content">
    <li>
      <a href="#change-password" onclick="$('#change-password').show();" class="grey-text text-darken-2 modal-trigger">
        <i class="fa fa-key" aria-hidden="true"></i> Change Password
      </a>
    </li>
    <li class="divider"></li>
    <li>
      <a href="#import-key" onclick="$('#import-key').show();" class="grey-text text-darken-2 modal-trigger">
        <i class="fa fa-key" aria-hidden="true"></i> Import key
      </a>
    </li>
    <li class="divider"></li>
    <li>
      <a href="#!" id="nav-url-logout" class="grey-text text-darken-2">
        <i class="fa fa-power-off" aria-hidden="true"></i> Logout
      </a>
    </li>
  </ul>
<?php }else{ ?>
  <ul id="user-dropdown" class="dropdown-content">
    <li>
      <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/login" class="grey-text text-darken-2">
        <i class="fa fa-unlock" aria-hidden="true"></i> Login
      </a>
    </li>
    <li>
      <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/register" class="grey-text text-darken-2">
        <i class="fa fa-file-text-o" aria-hidden="true"></i> Create a Wallet
      </a>
    </li>
  </ul>
<?php } ?>



<ul id="network-info-dropdown-mobile" class="dropdown-content">
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-code-fork" aria-hidden="true"></i>Magid Version: <span id="nav_WalletVersion-mobile">???</span>
    </a>
  </li>
  <li class="divider"></li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-link" aria-hidden="true"></i><span id="nav_blockCount-mobile">???</span> Blocks
    </a>
  </li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-cogs" aria-hidden="true"></i>diff POW: <span id="nav_diffPoW-mobile">???</span>
    </a>
  </li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-cogs" aria-hidden="true"></i>diff POS: <span id="nav_diffPoS-mobile">???</span>
    </a>
  </li>
  <li>
    <a href="#!" class="grey-text text-darken-2">
      <i class="fa fa-usd" aria-hidden="true"></i>Block Value: Σ <span id="nav_blockValue-mobile">???</span>
    </a>
  </li>
</ul>
<?php if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){ ?>
  <ul id="user-dropdown-mobile" class="dropdown-content">
    <li>
      <a href="#change-password" onclick="$('#change-password').show();" class="grey-text text-darken-2 modal-trigger">
        <i class="fa fa-key" aria-hidden="true"></i> Change Password
      </a>
    </li>
    <li class="divider"></li>
    <li>
      <a href="#import-key" onclick="$('#import-key').show();" class="grey-text text-darken-2 modal-trigger">
        <i class="fa fa-key" aria-hidden="true"></i> Import key
      </a>
    </li>
    <li class="divider"></li>
    <li>
      <a href="#!" id="nav-url-logout-mobile" class="grey-text text-darken-2">
        <i class="fa fa-power-off" aria-hidden="true"></i> Logout
      </a>
    </li>
  </ul>
<?php }else{ ?>
  <ul id="user-dropdown-mobile" class="dropdown-content">
    <li>
      <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/login" class="grey-text text-darken-2">
        <i class="fa fa-unlock" aria-hidden="true"></i> Login
      </a>
    </li>
    <li>
      <a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/register" class="grey-text text-darken-2">
        <i class="fa fa-file-text-o" aria-hidden="true"></i> Create a Wallet
      </a>
    </li>
  </ul>
<?php } ?>
