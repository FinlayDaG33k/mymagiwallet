<?php
  $appConfig = array(
    "MySQL" => array(
      "Host" => "127.0.0.1",
      "Username" => "mymagiwallet",
      "Password" => base64_encode("REDACTED"),
      "Database" => "MyMagiWallet"
    ),
    "Magid" => array(
      "Host" => "127.0.0.1",
      "Username" => "RPCUSERNAME",
      "Password" => "RPCPASSWORD",
      "Port" => 8332
    ),
    "Wallet" => array(
      "Key" => "WALLET ENCRYPTION KEY"
    )
  );
