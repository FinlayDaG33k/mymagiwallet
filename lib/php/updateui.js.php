<script>
  /* Update the wallet version */
  $.ajax({
    url: apiURL,
    method: "GET",
    data: "action=getWalletVersion",
    success: function (res) {
      data = JSON.parse(res);
      if(data.status == 200){
        $("#nav_WalletVersion").html(data.result);
      }else{
        $("#nav_WalletVersion").html("???");
      }
    }
  });


  function updateUI() {
    $("#navbtn_refreshing").css({ display: "block" });
    $("#navbtn_refreshing-mobile").css({ display: "block" });

    /* Update the connection count */
    if(UpdateUIItems.getConnections){
      $.ajax({
        url: apiURL,
        method: "GET",
        data: "action=getConnections",
        success: function (res) {
          data = JSON.parse(res);
          $("#nav_connectionCount").html(data.result);
          $("#navbtn_connectionCount").tooltip({tooltip: data.result + " connections to the Magi Network"});
          // Mobile sidenav
          $("#nav_connectionCount-mobile").html(data.result);
          $("#navbtn_connectionCount-mobile").tooltip({tooltip: data.result + " connections to the Magi Network"});
        }
      });
    }
    /* Update the blockchain info */
    if(UpdateUIItems.getMiningInfo){
      $.ajax({
        url: apiURL,
        method: "GET",
        data: "action=getMiningInfo",
        success: function (res) {
          data = JSON.parse(res);
          if(data.status == 200){
            $("#nav_blockCount").html(`${data.result.nodeblocks}/${data.result.netblocks}`);
			if((data.result.netblocks - data.result.nodeblocks) > 10){
				$("#syncing-banner").show();
			}else{
				$("#syncing-banner").hide();
			}
            $("#nav_diffPoW").html(data.result.difficulty.pow);
            $("#nav_diffPoS").html(data.result.difficulty.pos);
            $("#nav_blockValue").html(data.result.blockValue);
          }else{
            $("#nav_blockCount").html("???");
            $("#nav_diffPoW").html("???");
            $("#nav_diffPoS").html("???");
            $("#nav_blockValue").html("???");
          }
        }
      });
    }

    /* Update the transactions list */
    if(UpdateUIItems.getTransactions){
      $.ajax({
        url: apiURL,
        method: "POST",
        data: "action=getTransactions",
        success: function (res) {
          data = JSON.parse(res);
          if(data.status == 200){
            $("#table-transactions > tbody > tr").remove();
            $.each(data.result.transactions, function(tx, txdata) {
              var t = moment.unix(txdata.timestamp);
              switch(txdata.category){
                case "receive":
                  var txsign = "+";
                  var amount = txdata.amount;
                  break;
                case "send":
                  var txsign = "-";
                  var amount = (txdata.amount).toString().substr(1);
                  break;
                default:
                  var txsign = "?";
                  break;
              }

              $("<tr><td><a class=\"white-text\" href=\"<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/search/"+escapeHtml(txdata.txid)+"\">"+escapeHtml(txdata.txid.substr(0, 16))+"...</a></td><td><a class=\"white-text\" href=\"<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/search/"+escapeHtml(txdata.blockHash)+"\">"+escapeHtml(txdata.blockHash.substr(0, 16))+"...</td><td>"+escapeHtml(t.format("YYYY-MM-DD HH:mm:ss"))+"</td><td>"+escapeHtml(txdata.address)+"</td><td>"+escapeHtml(txsign)+" " + escapeHtml(amount) +"</td></tr>").prependTo("#table-transactions > tbody");
            });
          }
        }
      });
    }

    if(UpdateUIItems.getBalance){
      $.ajax({
        url: apiURL,
        method: "POST",
        data: "action=getBalance",
        success: function (res) {
          data = JSON.parse(res);
          if(data.status == 200){
            if(data.result.unconfirmed > 0){
              var unconfirmed =  " ("+escapeHtml(data.result.unconfirmed)+")";
            }else{
              var unconfirmed = "";
            }

            $("#wallet-balance").html(escapeHtml(data.result.confirmed));
            $("#wallet-balance-unconfirmed").html(unconfirmed);
          }else{
            $("#wallet-balance").html("???");
          }
        }
      });
    }

    if(UpdateUIItems.getTicker){
      $.ajax({
        url: apiURL,
        method: "GET",
        data: "action=getTicker",
        success: function (res) {
          data = JSON.parse(res);
          if(data.status == 200){
            $("#price-ticker").html(data.result[0].price_usd);
            $("#wallet-value").html(($("#wallet-balance").html() * data.result[0].price_usd).toFixed(2));
          }else{
            $("#price-ticker").html("???");
          }
        }
      });
    }

    if(UpdateUIItems.getWalletAddress){
      $.ajax({
        url: apiURL,
        method: "POST",
        data: "action=getWalletAddress",
        success: function (res) {
          data = JSON.parse(res);
          if(data.status == 200){
            $("#wallet-address").val(data.result);
          }else{
            $("#wallet-address").html("???");
          }
        }
      });
    }
    $("#navbtn_refreshing").css({ display: "none" });
    $("#navbtn_refreshing-mobile").css({ display: "none" });
  }

  $(document).ready(updateUI);
  setInterval(updateUI, 5000);

</script>
