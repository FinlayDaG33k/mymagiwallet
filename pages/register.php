<?php
  if(!empty($_SESSION['Authtoken'])){
    echo "you are already logged in!<br />Please log-out before creating a new wallet!";
  }else{
    ?>
      <div class="row">
        <form id="registerForm" class="col s12 m12 l12">
          <div class="row">
            <div class="input-field col s12">
              <input id="email" type="email" class="validate" name="email">
              <label for="email">Email</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="password" type="password" class="validate" name="password">
              <label for="password">Password</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="password_conf" type="password" class="validate">
              <label for="password_conf">Confirm Password</label>
              <small class="left"><a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/login">Already have a wallet?</a></small>
              <small class="right"><a href="<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/terms-of-service">By clicking Register, you agree to the Terms of Service</a></small>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light"><i class="fa fa-paper-plane" aria-hidden="true"></i> Register</button>
              <button type="reset" class="btn waves-effect waves-light"><i class="fa fa-times" aria-hidden="true"></i> reset</button>
            </div>
          </div>
        </form>
      </div>

      <script>
        $(document).ready(function() {
          $("#registerForm").submit(function(event) {
            event.preventDefault();
            if($("#password").val() == $("#password_conf").val()){
              $.ajax({
                url: apiURL,
                method: "POST",
                data: "action=register&" + $(this).serialize(),
                success: function (res) {
                  console.log(res);
                  data = JSON.parse(res);
                  if(data.status == 200){
                    Materialize.toast('Registration complete!<br />You may now login (should redirect after 2 seconds)', 4000);
                    window.setTimeout(function() {
                      location.href = "<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>/login";
                    }, 2000);
                  }else{
                    Materialize.toast(data.message, 4000);
                  }
                }
              });
            }else{
              Materialize.toast('Password does not match the confirmation password', 4000);
            }
          });
        });
      </script>
    <?php
  }
