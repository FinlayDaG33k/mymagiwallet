<?php
  if(!empty($_POST['email']) && !empty($_POST['password'])){
    $sql = "SELECT * FROM `Wallets` WHERE `Email`='".$conn->escape_string($_POST['email'])."';";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
      $wallet = $result->fetch_assoc();
      if(password_verify($_POST['password'],$wallet['Password'])){
        $authToken = $FinlayDaG33k->EzServer->randomStr(64);
        $sql = "UPDATE `Wallets` SET `Authtoken`='".$conn->escape_string($authToken)."' WHERE `Email`='".$conn->escape_string($_POST['email'])."';";
        if($conn->query($sql)){
          $_SESSION['wallet-identifier'] = $wallet['Wallet-Identifier'];
          $_SESSION['Authtoken'] = $authToken;
          $apiOutput = array("status" => 200, "message" => "Login success!");
        }else{
          $apiOutput = array("status" => 500, "message" => "Could not update Authtoken!");
        }
      }else{
        $apiOutput = array("status" => 403, "message" => "Email/Password invalid!");
      }
    }else{
      $apiOutput = array("status" => 403, "message" => "Email/Password invalid!");
    }
  }else{
    $apiOutput = array("status" => 501, "message" => "Email/Password can not be left blank!");
  }
