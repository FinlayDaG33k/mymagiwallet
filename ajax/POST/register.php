<?php
if(!empty($_POST['email'])){
  if(!empty($_POST['password'])){
    $sql = "SELECT * FROM `Wallets` WHERE `Email`='".$conn->escape_string($_POST['email'])."';";
    if($conn->query($sql)->num_rows == 0){
      // Generate a new wallet-identifier and check if it exists
      $walletId = $FinlayDaG33k->EzServer->randomStr(64);
      $sql = "SELECT `ID` FROM `Wallets` WHERE `Wallet-Identifier`='".$conn->escape_string($walletId)."';";
      $result = $conn->query($sql);

      // Keep regenerating and checking until a unique identifier is found
      while($result->num_rows > 0){
        $walletId = $FinlayDaG33k->EzServer->randomStr(64);
        $sql = "SELECT `ID` FROM `Wallets` WHERE `Wallet-Identifier`='".$conn->escape_string($walletId)."';";
        $result = $conn->query($sql);
      }

      // Generate a new wallet address
      $result = $bitcoin->getaccountaddress($walletId);
      // Check if our wallet address has been generated successfully and if so, insert the user
      if($result){
        $sql = "INSERT INTO `Wallets` (`ID`,`Email`,`Wallet-Identifier`,`Password`,`Authtoken`) VALUES (NULL,'".$conn->escape_string($_POST['email'])."','".$conn->escape_string($walletId)."','".$conn->escape_string(password_hash($_POST['password'],PASSWORD_DEFAULT))."','');";
        if($conn->query($sql)){
          $apiOutput = array("status" => 200, "result" => array("identifier" => $walletId,"address"=>$result));
        }else{
          $apiOutput = array("status" => 500, "message" => "Could not create new account!");
        }
      }else{
        $apiOutput = array("status" => 500, "message" => "Could not create wallet!");
      }
    }else{
      $apiOutput = array("status" => 403, "message" => "Email has already been taken!");
    }
  }else{
    $apiOutput = array("status" => 403, "message" => "Password cannot be left empty!");
  }
}else{
  $apiOutput = array("status" => 403, "message" => "Email cannot be left empty!");
}
