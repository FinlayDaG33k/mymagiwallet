<?php
  if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){
    if($_POST['amount'] > 0.001){
      if($bitcoin->validateaddress($_POST['recipient-address'])['isvalid']){
        $sql = "SELECT * FROM `Wallets` WHERE `Wallet-Identifier`='".$conn->escape_string($_SESSION['wallet-identifier'])."' AND `Authtoken`='".$conn->escape_string($_SESSION['Authtoken'])."';";
        $result = $conn->query($sql);
        if($result->num_rows > 0){
          if(password_verify($_POST['password'],$result->fetch_assoc()['Password'])){
            $result = $bitcoin->getbalance($_SESSION['wallet-identifier'],6);
            if($result + 0.0001 >= $_POST['amount']){
              $bitcoin->walletpassphrase($appConfig['Wallet']['Key'],60);
              $result = $bitcoin->sendfrom($_SESSION['wallet-identifier'],$_POST['recipient-address'],floatval ($_POST['amount']),6,$_POST['comment']);
              if(!empty($result)){
                $apiOutput = array("status" => 200, "result" => $result);
              }else{
                $apiOutput = array("status" => 500, "message" => "Unable to send coins!");
              }
            }else{
              $apiOutput = array("status" => 403, "message" => "Insufficient balance to send this transaction");
            }
          }else{
            $apiOutput = array("status" => 403, "message" => "Invalid password!");
          }
        }else{
          $apiOutput = array("status" => 403, "message" => "Invalid Wallet Identifier/Authtoken!");
        }
      }else{
        $apiOutput = array("status" => 403, "message" => "Invalid recipient!");
      }
    }else{
      $apiOutput = array("status" => 403, "message" => "send amount to low!");
    }
  }else{
    $apiOutput = array("status" => 403, "message" => "Wallet Identifier can't be blank!");
  }
