<?php
// create curl resource
$ch = curl_init();
// set url
curl_setopt($ch, CURLOPT_URL, "https://api.coinmarketcap.com/v1/ticker/magi/");
//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// $output contains the output string
$res = curl_exec($ch);
// close curl resource to free up system resources
curl_close($ch);
$data = json_decode($res,1);

if(!empty($res)){
  $apiOutput = array("status" => 200, "result" => $data);
}else{
  $apiOutput = array("status" => 500, "message" => "Could not update price ticker!");
}
?>
