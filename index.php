<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

// load config
require_once('config.inc.php');

// Load Class-FinlayDaG33k
require_once('lib/php/class-finlaydag33k/FinlayDaG33k.class.php');

// Start session
session_start();

// Connect to MySQL
$conn = $FinlayDaG33k->Database->Connect($appConfig['MySQL']);
if(!empty($_SESSION['wallet-identifier']) && !empty($_SESSION['Authtoken'])){
  $sql = "SELECT * FROM `Wallets` WHERE `Wallet-Identifier`='".$conn->escape_string($_SESSION['wallet-identifier'])."' AND `Authtoken`='".$conn->escape_string($_SESSION['Authtoken'])."';";
  if($conn->query($sql)->num_rows == 0){
    $_SESSION = array();
    header("Location: " . $FinlayDaG33k->EzServer->getHome() . "/login");
  }
}

?>
<html>
  <head>
    <?php include('components/head.php'); ?>
  </head>
  <body>
    <header>
      <?php include('components/navbar.php'); ?>
    </header>
    <main>
      <?php if($FinlayDaG33k->EzServer->getProto() != "https"){ ?>
        <script>Materialize.toast("You are not using a secure connection!<br />Proceed at your own risk!",4000);</script>
      <?php } ?>
      <div class="container">
		<div class="card-panel red lighten-4 red-text text-darken-4">
		  <b>Issues on our side!</b><br />
		  MyMagiWallet is currently undergoing issues regarding the blockchain not syncing up properly.<br />
		  Coins are safe, but please refrain from sending more coins towards your MyMagiWallet until further notice.<br />
		</div>
		<div class="card-panel red lighten-4 red-text text-darken-4">
		  <b>Shutdown Notice</b><br />
		  Due to low personal interest, continuous issues and lack of development, MyMagiWallet will shutdown on the first of June 2018.<br />
		  Please withdraw your coins before that date.<br />
		  If any issues arise that prevent you from doing so, please <a href="mailto:contact@finlaydag33k.nl">contact me</a> using the email address associated with your account, you will receive all privatekeys associated with your MyMagiWallet.<br />
		  Any requests that come from an email address <b>not</b> used for your MyMagiWallet will be ignored!<br />
		  Thanks for using MyMagiWallet!<br />
		  <br />
		  <strike>UPDATE: MyMagiWallet will potentially be handed off to a new startup I'm creating! (Nothing is sure yet)</strike>
		</div>
		<div class="card-panel orange lighten-4 orange-text text-darken-4" id="syncing-banner" style="display:none;">
		  <b>Syncing Up!</b><br />
		  MyMagiWallet is currently busy syncing up with the network!<br />
		  Transactions and balances might not be accurate
		</div>
        <?php include("pages/".$FinlayDaG33k->EzServer->getPage().".php"); ?>
      </div>
    </main>
    <footer class="page-footer indigo">
      <?php include('components/footer.php'); ?>
    </footer>
    <script src="lib/js/index.js?nocache=<?= htmlentities($FinlayDaG33k->EzServer->randomStr()); ?>"></script>
    <?php include("lib/php/updateui.js.php"); ?>
    <script>
      $(function(){
        $("#nav-url-logout").click(function(){
          $.ajax({
            url: apiURL,
            method: "POST",
            data: "action=logout",
            success: function (res) {
              data = JSON.parse(res);
              if(data.status == 200){
                Materialize.toast("Logged out successfully!<br />You will be redirected soon!",4000);
                window.location.href = "<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>";
              }
            }
          });
        });
        $("#nav-url-logout-mobile").click(function(){
          $.ajax({
            url: apiURL,
            method: "POST",
            data: "action=logout",
            success: function (res) {
              data = JSON.parse(res);
              if(data.status == 200){
                Materialize.toast("Logged out successfully!<br />You will be redirected soon!",4000);
                window.location.href = "<?= htmlentities($FinlayDaG33k->EzServer->getHome()); ?>";
              }
            }
          });
        })
      });
    </script>
    <?php
      if(!empty($_SESSION['Authtoken']) && $FinlayDaG33k->EzServer->getPage() == "wallet"){
        include('components/modals/wallet-send.php');
        include('components/modals/wallet-receive.php');
      }

      if(!empty($_SESSION['Authtoken'])){
        include('components/modals/change-password.php');
        include('components/modals/import-key.php');
      }
    ?>

  </body>
</html>
